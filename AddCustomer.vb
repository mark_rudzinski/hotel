﻿Public Class AddCustomer

    Friend custInfo As New List(Of List(Of String))
    Friend custInfoLabels As New List(Of String)
    Friend nextID As Integer = 100001
    Friend tCust As Integer = 0
    Dim saving As Boolean = False

    Private Sub CloseButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CloseButton.Click
        Me.Hide()
    End Sub

    Private Sub ClearButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClearButton.Click
        For Each x As TextBox In GroupBox1.Controls.OfType(Of TextBox)()
            x.Text = String.Empty
        Next
        For Each x As TextBox In GroupBox2.Controls.OfType(Of TextBox)()
            x.Text = String.Empty
        Next
        CustIDTextBox.Text = String.Empty
        CardTypeComboBox.Text = String.Empty
    End Sub

    Private Sub AddButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddButton.Click
        'Adds the customer info into a multi-dim list
        Try
            If FNTextBox.Text = String.Empty Or LNTextBox.Text = String.Empty Then
                Throw New System.Exception("First and last name must not be blank.")
            End If

            custInfo.Add(New List(Of String))
            If saving = False Then
                custInfo(tCust).Add(nextID.ToString())
                CustIDTextBox.Text = nextID
                nextID += 1
            Else
                custInfo(tCust).Add(CustIDTextBox.Text)
            End If
            For Each x As TextBox In GroupBox1.Controls.OfType(Of TextBox)()
                If x.Text = String.Empty Then
                    custInfo(tCust).Add("Empty")
                Else
                    custInfo(tCust).Add(x.Text)
                End If
            Next
            If CardNumTextBox.Text = String.Empty Then
                custInfo(tCust).Add("Empty")
            Else
                custInfo(tCust).Add(CardNumTextBox.Text)
            End If
            If CardTypeComboBox.Text = String.Empty Then
                custInfo(tCust).Add("Empty")
            Else
                custInfo(tCust).Add(CardTypeComboBox.Text)
            End If
            If ExpTextBox.Text = String.Empty Then
                custInfo(tCust).Add("Empty")
            Else
                custInfo(tCust).Add(ExpTextBox.Text)
            End If
            tCust += 1
            If saving = False Then
                MessageBox.Show("Customer info added to database.", "Customer Added", MessageBoxButtons.OK)
            End If
        Catch ex As Exception
            MessageBox.Show("Data Entry Error.", "Data Entry Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub SearchButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SearchButton.Click
        'Searches for customer info by customer ID
        Try
            Dim currentCust As New List(Of String)
            For Each x As List(Of String) In custInfo
                If x.Contains(CustIDTextBox.Text) Then
                    currentCust = x
                    Exit For
                End If
            Next

            Dim i As Integer = 1
            For Each x As TextBox In GroupBox1.Controls.OfType(Of TextBox)()
                x.Text = currentCust(i)
                i += 1
            Next
            CardNumTextBox.Text = currentCust(9)
            CardTypeComboBox.Text = currentCust(10)
            ExpTextBox.Text = currentCust(11)
        Catch ex As Exception
            MessageBox.Show("Incorrect customer ID number.", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'Deletes customer info 
        Try
            Dim i As Integer = 0
            For Each x As List(Of String) In custInfo
                If x.Contains(CustIDTextBox.Text) Then
                    custInfo.RemoveAt(i)
                    Exit For
                Else
                    i += 1
                End If
            Next
            tCust -= 1
            If saving = False Then
                ClearButton.PerformClick()
                MessageBox.Show("Customer info deleted from database.", "Customer Deleted", MessageBoxButtons.OK)
            End If
        Catch ex As Exception
            MessageBox.Show("Incorrect customer ID number.", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub SaveButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveButton.Click
        'Saves current customer info changes 
        Try
            saving = True
            Button2.PerformClick()
            AddButton.PerformClick()
            saving = False

            MessageBox.Show("Customer info saved.", "Info Saved", MessageBoxButtons.OK)
        Catch ex As Exception
            MessageBox.Show("Data Error.", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub PrintButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrintButton.Click
        PrintPreviewDialog1.Document = PrintDocument1
        PrintPreviewDialog1.ShowDialog()
    End Sub

    Friend Sub PrintDocument1_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        e.Graphics.DrawString("Customer Info", New Font("Arial", 20), Brushes.Black, 250, 20)
        Try
            Dim currentCust As New List(Of String)
            For Each x As List(Of String) In custInfo
                If x.Contains(CustIDTextBox.Text) Then
                    currentCust = x
                    Exit For
                End If
            Next
            e.Graphics.DrawString("Customer ID", New Font("Arial", 14), Brushes.Black, 20, 100)
            e.Graphics.DrawString(currentCust(0), New Font("Arial", 14), Brushes.Black, 200, 100)
            Dim nextLine As Integer = 30
            For i As Integer = 8 To 1 Step -1
                e.Graphics.DrawString(custInfoLabels(i - 1), New Font("Arial", 14), Brushes.Black, 20, (100 + nextLine))
                e.Graphics.DrawString(currentCust(i), New Font("Arial", 14), Brushes.Black, 200, (100 + nextLine))
                nextLine += 25
            Next
        Catch ex As Exception
            MessageBox.Show("Data Entry Error.", "Data Entry Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub AddCustomer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        For Each x As Label In GroupBox1.Controls.OfType(Of Label)()
            custInfoLabels.Add(x.Text)
        Next
    End Sub
End Class