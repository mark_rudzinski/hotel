﻿Public Class BookRoom

    Friend bookingInfo As New List(Of List(Of String))
    Friend nextBookingID As Integer = 10001
    Friend tBookRoom As Integer = 0
    Dim saving As Boolean = False

    Private Sub CloseButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CloseButton.Click
        Me.Hide()
    End Sub

    Private Sub ClearButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClearButton.Click
        For Each x As TextBox In GroupBox1.Controls.OfType(Of TextBox)()
            x.Text = String.Empty
        Next
        For Each x As TextBox In GroupBox2.Controls.OfType(Of TextBox)()
            x.Text = String.Empty
        Next
        RoomNameComboBox.Text = String.Empty
    End Sub

    Private Sub TextBox4_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DaysTextBox.TextChanged
        'calculates the price of the hotel
        Try
            Dim days As Integer = Integer.Parse(DaysTextBox.Text)
            Dim price As Decimal = Decimal.Parse(PriceTextBox.Text)

            TPriceTextBox.Text = "$ " + Str(price * days)
        Catch ex As Exception
        End Try
    End Sub

    Private Sub BookRoom_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub RoomNameComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RoomNameComboBox.SelectedIndexChanged
        Try
            Dim currentRoom As New List(Of String)
            For Each x As List(Of String) In Edit_Room.roomInfo
                If x.Contains(RoomNameComboBox.Text) Then
                    currentRoom = x
                    Exit For
                End If
            Next

            NameTextBox.Text = currentRoom(1)
            BedsTextBox.Text = currentRoom(2)
            PriceTextBox.Text = currentRoom(3)

        Catch ex As Exception
        End Try
    End Sub

    Private Sub AddButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddButton.Click
        'Adds the booking info into a multi-dim list
        Try
            If RoomNameComboBox.Text = String.Empty Or CustIDTextBox.Text = String.Empty Then
                Throw New System.Exception("Room name must not be blank.")
            End If

            bookingInfo.Add(New List(Of String))
            If saving = False Then
                bookingInfo(tBookRoom).Add(nextBookingID.ToString())
                ConIDTextBox.Text = nextBookingID
                nextBookingID += 1
            Else
                bookingInfo(tBookRoom).Add(ConIDTextBox.Text)
            End If

            bookingInfo(tBookRoom).Add(RoomNameComboBox.Text)
            bookingInfo(tBookRoom).Add(NameTextBox.Text)
            bookingInfo(tBookRoom).Add(BedsTextBox.Text)
            bookingInfo(tBookRoom).Add(PriceTextBox.Text)
            bookingInfo(tBookRoom).Add(CustIDTextBox.Text)
            bookingInfo(tBookRoom).Add(DaysTextBox.Text)
            bookingInfo(tBookRoom).Add(TPriceTextBox.Text)

            tBookRoom += 1
            RoomNameComboBox.Items.Remove(ConIDTextBox.Text)
            If saving = False Then
                MessageBox.Show("Booking info added to database.", "Booking Added", MessageBoxButtons.OK)
            End If
        Catch ex As Exception
            MessageBox.Show("Data Entry Error.", "Data Entry Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub CheckButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckButton.Click
        'checks to see if customer id is in database
        Try
            Dim found As Boolean = False
            For Each x As List(Of String) In AddCustomer.custInfo
                If x.Contains(CustIDTextBox.Text) Then
                    found = True
                    Exit For
                End If
            Next
            If found = False Then
                Throw New System.Exception("Customer not found.")
            End If
            MessageBox.Show("Customer is in database.", "Run Check", MessageBoxButtons.OK)
        Catch ex As Exception
            MessageBox.Show("Customer not in database.", "Data Entry Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            CustIDTextBox.Text = String.Empty
        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'deletes booking from database
        Try
            Dim i As Integer = 0
            For Each x As List(Of String) In bookingInfo
                If x.Contains(CustIDTextBox.Text) Then
                    bookingInfo.RemoveAt(i)
                    Exit For
                Else
                    i += 1
                End If
            Next
            tBookRoom -= 1
            If saving = False Then
                ClearButton.PerformClick()
                MessageBox.Show("Booking info deleted from database.", "Booking Deleted", MessageBoxButtons.OK)
            End If
        Catch ex As Exception
            MessageBox.Show("Incorrect Booking ID number.", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub SaveButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveButton.Click
        'Saves booking info
        Try
            saving = True
            Button2.PerformClick()
            AddButton.PerformClick()
            saving = False

            MessageBox.Show("Booking info saved.", "Info Saved", MessageBoxButtons.OK)
        Catch ex As Exception
            MessageBox.Show("Incorrect booking ID number.", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub PrintButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrintButton.Click
        PrintPreviewDialog1.Document = PrintDocument1
        PrintPreviewDialog1.ShowDialog()
    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim labelsArray() As String = {"Room Number", "Room Name", "Beds", "Price per night"}
        e.Graphics.DrawString("Booking Receipt", New Font("Arial", 20), Brushes.Black, 250, 20)
        Try
            Dim currentBooking As New List(Of String)
            For Each x As List(Of String) In bookingInfo
                If x.Contains(ConIDTextBox.Text) Then
                    currentBooking = x
                    Exit For
                End If
            Next
            Dim currentCustomer As New List(Of String)
            For Each y As List(Of String) In AddCustomer.custInfo
                If y.Contains(CustIDTextBox.Text) Then
                    currentCustomer = y
                    Exit For
                End If
            Next
            Dim currentRoom As New List(Of String)
            For Each z As List(Of String) In Edit_Room.roomInfo
                If z.Contains(RoomNameComboBox.Text) Then
                    currentRoom = z
                    Exit For
                End If
            Next

            e.Graphics.DrawString("Customer ID", New Font("Arial", 14), Brushes.Black, 20, 100)
            e.Graphics.DrawString(currentBooking(0), New Font("Arial", 14), Brushes.Black, 200, 100)
            e.Graphics.DrawString("Customer Name", New Font("Arial", 14), Brushes.Black, 20, 125)
            e.Graphics.DrawString(currentCustomer(7) + ", " + currentCustomer(8), New Font("Arial", 14), Brushes.Black, 200, 125)

            Dim nextLine As Integer = 25
            For i As Integer = 1 To 4
                e.Graphics.DrawString(labelsArray(i - 1), New Font("Arial", 14), Brushes.Black, 20, 125 + nextLine)
                e.Graphics.DrawString(currentBooking(i), New Font("Arial", 14), Brushes.Black, 200, 125 + nextLine)
                nextLine += 25
            Next

            e.Graphics.DrawString("Days Staying", New Font("Arial", 14), Brushes.Black, 20, 250)
            e.Graphics.DrawString(currentBooking(6), New Font("Arial", 14), Brushes.Black, 200, 250)
            e.Graphics.DrawString("Total Price", New Font("Arial", 14), Brushes.Black, 20, 275)
            e.Graphics.DrawString(currentBooking(7), New Font("Arial", 14), Brushes.Black, 200, 275)

            e.Graphics.DrawString("Comforts:", New Font("Arial", 14), Brushes.Black, 20, 325)

            For i As Integer = 11 To 4 Step -1
                e.Graphics.DrawString(Edit_Room.comfortsList(i - 4), New Font("Arial", 14), Brushes.Black, 20, 325 + nextLine)
                e.Graphics.DrawString(currentRoom(i), New Font("Arial", 14), Brushes.Black, 200, 325 + nextLine)
                nextLine += 25
            Next

        Catch ex As Exception
            MessageBox.Show("Data Entry Error.", "Data Entry Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub
End Class