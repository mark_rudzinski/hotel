﻿Public Class Edit_Room

    Friend roomInfo As New List(Of List(Of String))
    Friend comfortsList As New List(Of String)
    Friend nextRoomID As Integer = 101
    Friend tRoom As Integer = 0
    Dim saving As Boolean = False

    Private Sub CloseButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CloseButton.Click
        Me.Hide()
    End Sub

    Private Sub ClearButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClearButton.Click
        RoomIDTextBox.Text = String.Empty
        PriceTextBox.Text = String.Empty
        BedsComboBox.Text = String.Empty
        RoomNameComboBox.Text = String.Empty
        For Each x As CheckBox In GroupBox1.Controls.OfType(Of CheckBox)()
            x.Checked = False
        Next
    End Sub

    Private Sub AddButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AddButton.Click
        'Adds the customer info into a multi-dim list
        Try
            If RoomNameComboBox.Text = String.Empty Or BedsComboBox.Text = String.Empty Or PriceTextBox.Text = String.Empty Then
                Throw New System.Exception("Room name must not be blank.")
            End If

            roomInfo.Add(New List(Of String))
            If saving = False Then
                roomInfo(tRoom).Add(nextRoomID.ToString())
                RoomIDTextBox.Text = nextRoomID
                nextRoomID += 1
            Else
                roomInfo(tRoom).Add(RoomIDTextBox.Text)
            End If

            roomInfo(tRoom).Add(RoomNameComboBox.Text)
            roomInfo(tRoom).Add(BedsComboBox.Text)
            roomInfo(tRoom).Add(PriceTextBox.Text)

            For Each x As CheckBox In GroupBox1.Controls.OfType(Of CheckBox)()
                If x.Checked = True Then
                    roomInfo(tRoom).Add("Yes")
                Else
                    roomInfo(tRoom).Add("No")
                End If
            Next
            tRoom += 1
            BookRoom.RoomNameComboBox.Items.Add(RoomIDTextBox.Text)
            If saving = False Then
                MessageBox.Show("Room info added to database.", "Room Added", MessageBoxButtons.OK)
            End If
        Catch ex As Exception
            MessageBox.Show("Data Entry Error.", "Data Entry Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub SearchButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SearchButton.Click
        'Searches for rooms by ID
        Try
            Dim currentRoom As New List(Of String)
            For Each x As List(Of String) In roomInfo
                If x.Contains(RoomIDTextBox.Text) Then
                    currentRoom = x
                    Exit For
                End If
            Next

            RoomNameComboBox.Text = currentRoom(1)
            BedsComboBox.Text = currentRoom(2)
            PriceTextBox.Text = currentRoom(3)

            Dim i As Integer = 4
            For Each x As CheckBox In GroupBox1.Controls.OfType(Of CheckBox)()
                If currentRoom(i) = "Yes" Then
                    x.Checked = True
                Else
                    x.Checked = False
                End If
                i += 1
            Next

        Catch ex As Exception
            MessageBox.Show("Incorrect customer ID number.", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'Deletes room info 
        Try
            Dim i As Integer = 0
            For Each x As List(Of String) In roomInfo
                If x.Contains(RoomIDTextBox.Text) Then
                    roomInfo.RemoveAt(i)
                    Exit For
                Else
                    i += 1
                End If
            Next
            tRoom -= 1
            BookRoom.RoomNameComboBox.Items.Remove(RoomIDTextBox.Text)
            If saving = False Then
                ClearButton.PerformClick()
                MessageBox.Show("Room info deleted from database.", "Room Deleted", MessageBoxButtons.OK)
            End If
        Catch ex As Exception
            MessageBox.Show("Incorrect Room ID number.", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub SaveButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveButton.Click
        'Saves room info
        Try
            saving = True
            Button2.PerformClick()
            AddButton.PerformClick()
            saving = False

            MessageBox.Show("Room info saved.", "Info Saved", MessageBoxButtons.OK)
        Catch ex As Exception
            MessageBox.Show("Incorrect room ID number.", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub Edit_Room_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        For Each x As CheckBox In GroupBox1.Controls.OfType(Of CheckBox)()
            comfortsList.Add(x.Text)
        Next
    End Sub
End Class